//* This file is part of the MOOSE framework
//* https://www.mooseframework.org
//*
//* All rights reserved, see COPYRIGHT for full restrictions
//* https://github.com/idaholab/moose/blob/master/COPYRIGHT
//*
//* Licensed under LGPL 2.1, please see LICENSE for details
//* https://www.gnu.org/licenses/lgpl-2.1.html
#include "penguinTestApp.h"
#include "penguinApp.h"
#include "Moose.h"
#include "AppFactory.h"
#include "MooseSyntax.h"
#include "ModulesApp.h"

InputParameters
penguinTestApp::validParams()
{
  InputParameters params = penguinApp::validParams();
  return params;
}

penguinTestApp::penguinTestApp(InputParameters parameters) : MooseApp(parameters)
{
  penguinTestApp::registerAll(
      _factory, _action_factory, _syntax, getParam<bool>("allow_test_objects"));
}

penguinTestApp::~penguinTestApp() {}

void
penguinTestApp::registerAll(Factory & f, ActionFactory & af, Syntax & s, bool use_test_objs)
{
  penguinApp::registerAll(f, af, s);
  if (use_test_objs)
  {
    Registry::registerObjectsTo(f, {"penguinTestApp"});
    Registry::registerActionsTo(af, {"penguinTestApp"});
  }
}

void
penguinTestApp::registerApps()
{
  registerApp(penguinApp);
  registerApp(penguinTestApp);
}

/***************************************************************************************************
 *********************** Dynamic Library Entry Points - DO NOT MODIFY ******************************
 **************************************************************************************************/
// External entry point for dynamic application loading
extern "C" void
penguinTestApp__registerAll(Factory & f, ActionFactory & af, Syntax & s)
{
  penguinTestApp::registerAll(f, af, s);
}
extern "C" void
penguinTestApp__registerApps()
{
  penguinTestApp::registerApps();
}
