[Mesh]
  type = GeneratedMesh
  dim = 2
  nx  = 100
  ny  = 10
  xmax = 0.304   #Height of the chamber
  ymax = 0.0257  #Test chamber radius
[]



[Variables]
 [pressure]
 []
[]



[Kernels]
  [diffusion]
     type     = DarcyPressure
     variable = pressure
  []
[]



[BCs]
  [inlet]
     type = DirichletBC
     variable = pressure
     boundary = left
     value = 4000
  []
  [outlet]
     type = DirichletBC
     variable = pressure
     boundary = right
     value = 0
  []
[]


[Materials]
  [column]
    type= PackedColumn
    radius = '1 + 2/3.04*x'
    outputs = exodus
  []
[]

[Problem]
  type = FEProblem
  coord_type = RZ
  rz_coord_axis = X # Which axis is the symmetry around
[]




[Executioner]
  type = Steady
  solve_type = NEWTON
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomerang'
[]





[Outputs]
  exodus = true

[]
