[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 100
  ny = 10
  xmax = 0.304 # Height of the chamber
  ymax = 0.0257 # Test chamber radius
[]

[Variables]
  [temperature]
  []
[]

[Kernels]
  [heat_conduction]
    type = ADHeatConduction
    variable = temperature
  []
[]

[BCs]
  [inlet_temperature]
    type = DirichletBC
    variable = temperature
    boundary = 'left'
    value = 350
  []
  [outlet_temperature]
    type = DirichletBC
    variable = temperature
    boundary = 'right'
    value = 300
  []
[]

[Materials]
  [steel]
    type = ADGenericConstantMaterial
    prop_names = 'thermal_conductivity'
    prop_values = '18' # k
  []
[]

[Problem]
  type = FEProblem
  coord_type = 'RZ'
  rz_coord_axis = X # Which axis is the symmetry around
[]

[Executioner]
  type = Steady
  solve_type = NEWTON
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomerang'
[]

[Outputs]
  exodus = true
[]
