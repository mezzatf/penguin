#pragma once

#include "ADKernel.h"

/**
 *
 */
class  DarcyPressure : public ADKernel
{

public:
  static InputParameters validParams();
  DarcyPressure(const InputParameters & parameters);

protected:
  virtual ADReal computeQpResidual() override;

  // Before Material
  // const Real & _permeability;
  // const Real & _viscosity;

  // After adding the material
  const ADMaterialProperty<Real> & _permeability;
  const ADMaterialProperty<Real> & _viscosity;

};
